'use strict';

angular.module('lysander.shellApp')
  .factory('Service', function ($rootScope, $http, config) {
    
    var Service = function(method, path, auth) {
      var me = this;
      this.method = method;
      this.path = path;
      this.params = {};
      this.data = {};
      this.rows = 6;
      this.error = false;
      this.response = null;      
      this.onSuccess = null;
      this.onError = null;

      if(auth) {
        if($rootScope.token) {
          this.params.token = $rootScope.token.id;
        }
        $rootScope.$watch('token', function(newValue) {
          if(newValue) {
            me.params.token = newValue.id;
          }
        });
      }
    };

    Service.prototype.url = function() {
      var url = config.apiUrl + this.path;
      if(_.keys(this.params).length > 0) {
        url += '?' + $.param(this.params);
      }
      return url;
    };

    Service.prototype.send = function() {
      var me = this;
      var prom = $http({ method:this.method, url:this.url(), data:this.data })
        .then(function(response) {
          me.error = false;
          me.response = response.data;
          me.onSuccess(response.data);
        }, function(err) {
          me.error = true;
          me.response = err.data;
          me.onError(response.data);
        });
      return prom;
    };

    Service.prototype.on = function(type, func) {
      if(type == 'success') {
        this.onSuccess = func;
      } else if(type == 'error') {
        this.onError = func;
      }
    }

    return Service;

  });
